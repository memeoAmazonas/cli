# CLI Gitlab Farabela

Proyecto para la creación de repositorios a travez de una plantilla.

## Comenzando 🚀

### Pre-requisitos 📋

## Construido con 🛠️

* [Go kit](https://github.com/go-kit/kit) - A standard library for microservices.
* [gorilla/mux](https://github.com/gorilla/mux) - A powerful HTTP router and URL matcher for building Go web servers with 🦍
* [jwt-go](https://github.com/dgrijalva/jwt-go) - Golang implementation of JSON Web Tokens (JWT).
* [Go/Golang package for Crontab tickers](https://github.com/mileusna/crontab) - Go/Golang crontab ticker.
* [swag](https://github.com/swaggo/swag) - Automatically generate RESTful API documentation with Swagger 2.0 for Go.
* [go-gitlab](https://github.com/xanzy/go-gitlab) - A GitLab API client enabling Go programs to interact with GitLab in a simple and uniform way.
* [MongoDB Go Driver](https://pkg.go.dev/go.mongodb.org/mongo-driver) - The MongoDB supported driver for Go.

## Desarrollo

Se recomienda el uso de la librería [Air](https://github.com/go-kit/kit)

### Swagger

API Rest documentada con OpenAPI 2.0

http://{url}/docs/

## Despliegue 📦

### Compilación
```
go build -o cli cmd/main.go
```

### Docker
```
docker build  -t "cli:dockerfile" .
```

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://gitlab.com/memeoAmazonas/cli/-/tags).