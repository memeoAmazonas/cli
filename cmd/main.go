package main

import (
	"cli-falabella/docs"
	_ "cli-falabella/docs"
	"cli-falabella/internal/auth"
	"cli-falabella/internal/cli/config"
	"cli-falabella/internal/cli/groups"
	"cli-falabella/internal/cli/repositories"
	"cli-falabella/internal/database"
	"cli-falabella/internal/users"
	"context"
	"flag"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/rs/cors"
	"github.com/spf13/viper"
	httpSwagger "github.com/swaggo/http-swagger"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// @title API Project Tool
// @version 1.0.0
// @description API Project Tool
// @termsOfService http://swagger.io/terms/

// @contact.email cli@falabella.cl

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /api

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	var logger log.Logger
	{
		/*file, _ := os.OpenFile("./logs/log-" + time.Now().Format("2006-01-02") +".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		logger = log.NewLogfmtLogger(log.NewSyncWriter(file))*/
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"main", "caller",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	viper.SetConfigFile("config.json")
	err := viper.ReadInConfig()
	if err != nil {
		level.Error(logger).Log("exit", err)
		os.Exit(-1)
	}

	var httpAddr = flag.String("http", fmt.Sprintf(":%s", viper.GetString("PORT")), "HTTP listen address")

	level.Info(logger).Log("message", "main started")
	defer level.Info(logger).Log("message", "main end")
	ctx := context.Background()
	var db *mongo.Database
	{
		var err error
		db, err = database.GetDatabase()
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
	}
	flag.Parse()
	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	// ROUTERS
	serveMux := http.NewServeMux()
	serveMux.Handle("/api/auth/", auth.NewHttpServer(ctx, auth.Server(db, logger), logger))
	serveMux.Handle("/api/users/", users.NewHttpServer(ctx, users.Server(db, logger), logger))
	serveMux.Handle("/api/cli/config/", config.NewHttpServer(ctx, config.Server(db, logger), logger))
	serveMux.Handle("/api/cli/templates/", repositories.NewHttpServer(ctx, repositories.Server(db, logger), logger))
	serveMux.Handle("/api/cli/groups/", groups.NewHttpServer(ctx, groups.Server(db, logger), logger))

	// SWAGGER
	docs.SwaggerInfo.Host = viper.GetString("HOST") + ":" + viper.GetString("PORT")
	docs.SwaggerInfo.Description = "API Falabella gestionada para el repositorio de <b>" + viper.GetString("URL_GITLAB_BASE") + "</b>."
	docs.SwaggerInfo.Schemes = []string{"http", "https"}
	serveMux.Handle("/docs/", httpSwagger.WrapHandler)

	corsOpts := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},
		AllowedHeaders: []string{
			"*",
		},
	})

	go func() {
		handler := cors.Default().Handler(serveMux)
		http.Handle("/", serveMux)
		fmt.Println("listen on port", *httpAddr)
		errs <- http.ListenAndServe(*httpAddr, corsOpts.Handler(handler))
	}()
	level.Error(logger).Log("exit", <-errs)
}
