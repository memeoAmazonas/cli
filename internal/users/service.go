package users

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/helpers/password"
	"cli-falabella/internal/security"
	"context"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Service interface {
	GetAllUsers(ctx context.Context, res entity.RequestHttp) ([]Users, error)
	GetUser(ctx context.Context, res entity.RequestHttp) (*Users, error)
	CreateUser(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error)
	UpdateUser(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error)
	DeleteUser(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error)
}

type service struct {
	repository Repository
	logger     log.Logger
}

func NewService(repository Repository, logger log.Logger) *service {
	users, _ := repository.GetAllUsers(context.Background())
	
	if len(users) == 0 {
		psswrd, err := password.HashPassword("admin")
		if err != nil {
			level.Error(logger).Log("password", err.Error())
		}

		err = repository.CreateUser(context.Background(), User{
			ID:         primitive.NewObjectID(),
			Username:   "admin",
			Password:   psswrd,
			Name:       "Administrador",
			Active:     true,
			LastAccess: time.Now(),
		})
		if err != nil {
			level.Error(logger).Log("error", err.Error())
		}
	}
	
	return &service{
		repository: repository,
		logger:     logger,
	}
}

// GetAllUsers godoc
// @Summary Returns all users system
// @Description Returns all users system.
// @ID get-users-all
// @Tags Users
// @Accept  json
// @Produce  json
// @Success 200 {array} Users
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /users [get]
func (s service) GetAllUsers(ctx context.Context, res entity.RequestHttp) ([]Users, error) {
	logger := log.With(s.logger, "service", "getallusers")
	users, err := s.repository.GetAllUsers(ctx)
	if err != nil {
		level.Error(logger).Log("error", err.Error())
		return nil, entity.ErrRepositoryUsers
	}

	var data []Users
	data = []Users{}
	for index := 0; index < len(users); index++ {
		data = append(data, Users{
			Identifier: users[index].ID.Hex(),
			Username:   users[index].Username,
			Name:       users[index].Name,
			Active:     users[index].Active,
		})
	}

	level.Info(logger).Log("list", "ok")
	return data, nil
}

// GetUser godoc
// @Summary Returns users system
// @Description Returns users system.
// @ID get-users-id
// @Tags Users
// @Accept  json
// @Produce  json
// @Param   id      path   string     true  "Users ID"
// @Success 200 {object} Users
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /users/{id} [get]
func (s service) GetUser(ctx context.Context, res entity.RequestHttp) (*Users, error) {
	logger := log.With(s.logger, "service", "getusers")

	claims := res.Claims.(*security.JwtClaim)
	idUsers := res.Params.(string)
	logger = log.With(s.logger, "users", claims.Username)

	user, err := s.repository.GetUserID(ctx, idUsers)
	if err != nil {
		level.Error(logger).Log("user", err.Error())
		return nil, entity.ErrRepositoryUsers
	}

	return &Users{
		Identifier: user.ID.Hex(),
		Username:   user.Username,
		Name:       user.Name,
		Active:     user.Active,
	}, nil
}

// CreateUser godoc
// @Summary Create users system
// @Description Create users system.
// @ID post-create-users
// @Tags Users
// @Accept  json
// @Produce  json
// @Param Users body UsersCreate true "Users create"
// @Success 201 {object} entity.ResponseHttp
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /users/create [post]
func (s service) CreateUser(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error) {
	logger := log.With(s.logger, "service", "create")

	claims := res.Claims.(*security.JwtClaim)
	request := res.Request.(UsersCreate)
	logger = log.With(s.logger, "users", claims.Username)

	if request.Password1 != request.Password2 {
		level.Error(logger).Log("password", "password error")
		return nil, entity.ErrPassword
	}

	countUser, err := s.repository.CountUserName(ctx, request.Username)
	if err != nil {
		level.Error(logger).Log("user", err.Error())
		return nil, entity.ErrRepositoryUsers
	}
	if *countUser > 0 {
		level.Error(logger).Log("user", "users exist")
		return nil, entity.ErrRepositoryUsersExist
	}

	pswd, err := password.HashPassword(request.Password1)
	if err != nil {
		level.Error(logger).Log("password", err.Error())
		return nil, entity.ErrPasswordEncode
	}

	// create user
	user := User{
		ID:         primitive.NewObjectID(),
		Username:   request.Username,
		Password:   pswd,
		Name:       request.Name,
		Active:     true,
		LastAccess: time.Now(),
	}
	err = s.repository.CreateUser(ctx, user)
	if err != nil {
		level.Error(logger).Log("create", err.Error())
		return nil, entity.ErrRepositoryCreateUsers
	}

	// eliminar el usuario administrador
	users, err := s.repository.GetAllUsers(ctx)
	if err != nil {
		level.Error(logger).Log("get all", "ok")
	}
	if len(users) > 1 {
		countUser, err = s.repository.CountUserName(ctx, "admin")
		if err != nil {
			level.Error(logger).Log("count", "admin")
		}
		userAdmin, err := s.repository.GetUserName(ctx, "admin")
		if err != nil {
			level.Error(logger).Log("get", "admin")
		}
		if *countUser > 0 {
			err = s.repository.DeleteUser(ctx, userAdmin.ID.Hex())
			if err != nil {
				level.Error(logger).Log("delete", "admin", "error", err.Error())
			}
			level.Info(logger).Log("delete", "admin", "status", "ok")
		}
	}

	return &entity.ResponseHttp{
		Code:    201,
		Key:     "USERS_CREATE",
		Message: "User create",
		Data:    user,
	}, nil
}

// UpdateUser godoc
// @Summary Update users system
// @Description Update users system.
// @Tags Users
// @Accept  json
// @Produce  json
// @Param   id      path   string     true  "Users ID"
// @Param Users body UsersUpdate true "Users update"
// @Success 200 {object} entity.ResponseHttp
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /users/{id} [patch]
func (s service) UpdateUser(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error) {
	logger := log.With(s.logger, "service", "update")

	claims := res.Claims.(*security.JwtClaim)
	request := res.Request.(UsersUpdate)
	idUsers := res.Params.(string)
	logger = log.With(s.logger, "users", claims.Username)

	countUser, err := s.repository.CountUserID(ctx, idUsers)
	if err != nil {
		level.Error(logger).Log("user", err.Error())
		return nil, entity.ErrRepositoryUsers
	}
	if *countUser == 0 {
		level.Error(logger).Log("user", "users exist")
		return nil, entity.ErrRepositoryUsersNotFound
	}

	err = s.repository.UpdateUser(ctx, idUsers, request)
	if err != nil {
		level.Error(logger).Log("update", err.Error())
		return nil, entity.ErrRepositoryUsersUpdate
	}

	return &entity.ResponseHttp{
		Code:    200,
		Key:     "USERS_UPDATE",
		Message: "Users update",
		Data:    request,
	}, nil
}

// DeleteUser godoc
// @Summary Delete users system
// @Description Delete users system.
// @Tags Users
// @Accept  json
// @Produce  json
// @Param   id      path   string     true  "Users ID"
// @Success 200 {object} entity.ResponseHttp
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /users/{id} [delete]
func (s service) DeleteUser(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error) {
	logger := log.With(s.logger, "service", "update")

	claims := res.Claims.(*security.JwtClaim)
	idUsers := res.Params.(string)
	logger = log.With(s.logger, "users", claims.Username)

	countUser, err := s.repository.CountUserID(ctx, idUsers)
	if err != nil {
		level.Error(logger).Log("user", err.Error())
		return nil, entity.ErrRepositoryUsers
	}
	if *countUser == 0 {
		level.Error(logger).Log("user", "users not exist")
		return nil, entity.ErrRepositoryUsersNotFound
	}

	err = s.repository.DeleteUser(ctx, idUsers)
	if err != nil {
		level.Error(logger).Log("delete", err.Error())
		return nil, entity.ErrRepositoryUsersDelete
	}

	return &entity.ResponseHttp{
		Code:    200,
		Key:     "USERS_DELETE",
		Message: "Users delete",
		Data:    idUsers,
	}, nil
}
