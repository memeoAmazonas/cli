package users

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/security"
	"context"
	"encoding/json"
	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

type Users struct {
	Identifier string `json:"identifier"`
	Username   string `json:"username"`
	Name       string `json:"name"`
	Active     bool   `json:"active"`
}

type UsersCreate struct {
	Username  string `json:"username" validate:"required" example:"usuario1"`
	Name      string `json:"name" validate:"required" example:"Juan Jose Garcia"`
	Password1 string `json:"password1" validate:"required"`
	Password2 string `json:"password2" validate:"required"`
}

type UsersUpdate struct {
	Name   string `json:"name" validate:"required" example:"Juan Jose Garcia"`
	Active bool   `json:"active" validate:"required" example:"true"`
}

func DecodeRequestUsersList(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	return entity.RequestHttp{Claims: claims}, nil
}

func DecodeRequestGetUser(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	vars := mux.Vars(r)

	return entity.RequestHttp{Claims: claims, Params: vars["id"]}, nil
}

func DecodeRequestCreateUser(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	var req UsersCreate
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, entity.ErrValidation
	}

	// validation
	validate := validator.New()
	err = validate.Struct(req)
	if err != nil {
		return nil, entity.ErrValidation
	}

	return entity.RequestHttp{Request: req, Claims: claims}, nil
}

func DecodeRequestUpdateUser(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	var req UsersUpdate
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, entity.ErrValidation
	}

	vars := mux.Vars(r)
	if primitive.IsValidObjectID(vars["id"]) == false {
		return nil, entity.ErrValidation
	}

	return entity.RequestHttp{
		Claims:  claims,
		Params:  vars["id"],
		Request: req,
	}, nil
}

func DecodeRequestDeleteUser(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	vars := mux.Vars(r)
	if primitive.IsValidObjectID(vars["id"]) == false {
		return nil, entity.ErrValidation
	}

	return entity.RequestHttp{
		Claims: claims,
		Params: vars["id"],
	}, nil
}
