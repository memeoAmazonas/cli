package users

import (
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/mongo"
)

func Server(db *mongo.Database, logger log.Logger) Endpoint {
	{
		logger = log.With(
			logger,
			"service", "users",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}
	level.Info(logger).Log("message", "users service started")


	var srv Service
	{
		repository := NewRepository(db, logger)
		srv = NewService(repository, logger)
	}

	return MakeEndpoint(srv)
}
