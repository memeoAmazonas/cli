package users

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/middleware"
	"context"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"net/http"
)

func NewHttpServer(_ context.Context, endpoint Endpoint, logger log.Logger) http.Handler {
	opts := []httptransport.ServerOption{
		httptransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		httptransport.ServerErrorEncoder(entity.EncodeError),
	}

	r := mux.NewRouter()
	userRouter := r.PathPrefix("/api/users/").Subrouter()

	// get all users
	userRouter.Path("/").Handler(middleware.ProtectedAdmin(httptransport.NewServer(
		endpoint.GetAllUsers,
		DecodeRequestUsersList,
		entity.EncodeResponse,
		opts...,
	))).Methods("GET")

	// get users
	userRouter.Path("/{id}").Handler(middleware.ProtectedAdmin(httptransport.NewServer(
		endpoint.GetUser,
		DecodeRequestGetUser,
		entity.EncodeResponse,
		opts...,
	))).Methods("GET")

	// create users
	userRouter.Path("/create").Handler(middleware.ProtectedAdmin(httptransport.NewServer(
		endpoint.CreateUser,
		DecodeRequestCreateUser,
		entity.EncodeResponse,
		opts...,
	))).Methods("POST")

	// update users
	userRouter.Path("/{id}").Handler(middleware.ProtectedAdmin(httptransport.NewServer(
		endpoint.UpdateUser,
		DecodeRequestUpdateUser,
		entity.EncodeResponse,
		opts...,
	))).Methods("PATCH")

	// delete users
	userRouter.Path("/{id}").Handler(middleware.ProtectedAdmin(httptransport.NewServer(
		endpoint.DeleteUser,
		DecodeRequestDeleteUser,
		entity.EncodeResponse,
		opts...,
	))).Methods("DELETE")

	cors.Default().Handler(userRouter)
	return middleware.HeaderMiddleware(userRouter)
}
