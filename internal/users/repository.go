package users

import (
	"cli-falabella/internal/helpers/password"
	"context"
	"errors"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type User struct {
	ID         primitive.ObjectID `json:"id,omitempty" bson:"_id" `
	Username   string             `json:"username" bson:"username"`
	Password   string             `json:"-" bson:"password"`
	Name       string             `json:"name" bson:"name"`
	Active     bool               `json:"active" bson:"active"`
	LastAccess time.Time          `json:"last_access" bson:"last_access"`
}

type Repository interface {
	CountUserID(ctx context.Context, idUser string) (*int64, error)
	CountUserName(ctx context.Context, username string) (*int64, error)
	GetUserName(ctx context.Context, username string) (*User, error)
	GetUserID(ctx context.Context, id string) (*User, error)
	GetAllUsers(ctx context.Context) ([]User, error)
	CreateUser(ctx context.Context, user User) error
	UpdateUser(ctx context.Context, idUser string, user UsersUpdate) error
	UpdateLastAccess(ctx context.Context, username string) error
	DeleteUser(ctx context.Context, idUser string) error
}

type repository struct {
	db     *mongo.Database
	logger log.Logger
}

const COLLECTION = "users"

var ERROR = errors.New("Unable to handle repo request")

func NewRepository(db *mongo.Database, logger log.Logger) Repository {
	return &repository{
		db:     db,
		logger: log.With(logger, "repository", fmt.Sprintf("collection %s", COLLECTION)),
	}
}

func (r *repository) CountUserID(ctx context.Context, idUser string) (*int64, error) {
	r.logger.Log("count", "auth")
	objectid, err := primitive.ObjectIDFromHex(idUser)
	if err != nil {
		level.Error(r.logger).Log("parse", err.Error())
		return nil, errors.New("parse error object id")
	}
	count, err := r.db.Collection(COLLECTION).CountDocuments(ctx, bson.M{"_id": objectid})
	if err != nil {
		return nil, err
	}

	return &count, nil
}

func (r *repository) CountUserName(ctx context.Context, username string) (*int64, error) {
	r.logger.Log("count", "auth")
	count, err := r.db.Collection(COLLECTION).CountDocuments(ctx, bson.M{"username": username})
	if err != nil {
		return nil, err
	}

	return &count, nil
}

func (r *repository) GetUserName(ctx context.Context, username string) (*User, error) {
	user := User{}
	if err := r.db.Collection(COLLECTION).FindOne(ctx, bson.M{"username": username}).Decode(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *repository) GetUserID(ctx context.Context, id string) (*User, error) {
	user := User{}
	objectid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		level.Error(r.logger).Log("parse", err.Error())
		return nil, errors.New("parse error object id")
	}
	if err := r.db.Collection(COLLECTION).FindOne(ctx, bson.M{"_id": objectid}).Decode(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *repository) GetAllUsers(ctx context.Context) ([]User, error) {
	var users []User
	users = []User{}

	cursor, err := r.db.Collection(COLLECTION).Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	for cursor.Next(ctx) {
		var user User
		cursor.Decode(&user)
		users = append(users, user)
	}

	return users, nil
}

func (r *repository) CreateUser(ctx context.Context, user User) error {
	r.logger.Log("create", "auth")
	countUser, err := r.CountUserName(ctx, user.Username)
	if err != nil {
		level.Error(r.logger).Log("error", err.Error())
		return err
	}
	if *countUser == 0 {
		_, err = r.db.Collection(COLLECTION).InsertOne(ctx, user)
		if err != nil {
			return errors.New("error insert auth")
		}
		level.Info(r.logger).Log("auth", user.Username, "create auth", "ok")

		return nil
	}

	users, err := r.GetAllUsers(ctx)
	if err != nil {
		level.Error(r.logger).Log("list", "users", "error", err.Error())
	}
	if len(users) > 0 {
		countUser, err := r.CountUserName(ctx, "admin")
		if err != nil {
			level.Error(r.logger).Log("count", "admin")
		}
		if *countUser > 0 {
			userAdmin, err := r.GetUserName(ctx, "admin")
			if err != nil {
				level.Error(r.logger).Log("get", "admin")
			}
			err = r.DeleteUser(ctx, userAdmin.ID.Hex())
			if err != nil {
				level.Error(r.logger).Log("delete", "admin", "error", err.Error())
			}
			level.Info(r.logger).Log("delete", "admin", "status", "ok")
		}
	}

	level.Info(r.logger).Log("auth exist", user.Username)
	return errors.New("users exist")
}

func (r *repository) UpdateUser(ctx context.Context, idUser string, user UsersUpdate) error {
	objectid, err := primitive.ObjectIDFromHex(idUser)
	if err != nil {
		level.Error(r.logger).Log("parse", err.Error())
		return errors.New("parse error object id")
	}

	_, err = r.db.Collection(COLLECTION).UpdateOne(ctx, bson.M{"_id": objectid}, bson.D{
		{"$set", bson.D{{"name", user.Name}}},
		{"$set", bson.D{{"active", user.Active}}},
	})
	if err != nil {
		level.Error(r.logger).Log("update", err.Error())
		return errors.New("users not update")
	}

	level.Error(r.logger).Log("update", "ok")
	return nil
}

func (r *repository) UpdateLastAccess(ctx context.Context, username string) error {
	level.Info(r.logger).Log("update", "access")
	_, err := r.db.Collection(COLLECTION).UpdateOne(ctx, bson.M{"username": username}, bson.D{
		{"$set", bson.D{{"last_access", time.Now()}}},
	})
	if err != nil {
		level.Error(r.logger).Log("auth", username, "error", err.Error())
		return err
	}

	level.Info(r.logger).Log("update", "ok")
	return nil
}

func (r *repository) DeleteUser(ctx context.Context, idUser string) error {
	objectid, err := primitive.ObjectIDFromHex(idUser)
	if err != nil {
		level.Error(r.logger).Log("parse", err.Error())
		return errors.New("parse error object id")
	}

	level.Info(r.logger).Log("delete", "access")
	_, err = r.db.Collection(COLLECTION).DeleteOne(ctx, bson.M{"_id": objectid})
	if err != nil {
		level.Error(r.logger).Log("id", idUser, "error", err.Error())
		return err
	}

	// creando usuario por defecto
	users, err := r.GetAllUsers(ctx)
	if err != nil {
		level.Error(r.logger).Log("list", "users", "error", err.Error())
	}
	if len(users) == 0 {
		psswrd, err := password.HashPassword("admin")
		if err != nil {
			level.Error(r.logger).Log("password", err.Error())
		}
		err = r.CreateUser(ctx, User{
			ID:         primitive.NewObjectID(),
			Username:   "admin",
			Password:   psswrd,
			Name:       "Administrador",
			Active:     true,
			LastAccess: time.Now(),
		})
		if err != nil {
			level.Error(r.logger).Log("user", "create", "error", err.Error())
		}
		level.Info(r.logger).Log("create", "user", "status", "ok")
	}

	level.Info(r.logger).Log("delete", "ok")
	return nil
}
