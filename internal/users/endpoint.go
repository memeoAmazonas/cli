package users

import (
	"cli-falabella/internal/entity"
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoint struct {
	GetAllUsers endpoint.Endpoint
	GetUser     endpoint.Endpoint
	CreateUser  endpoint.Endpoint
	UpdateUser  endpoint.Endpoint
	DeleteUser  endpoint.Endpoint
}

func MakeEndpoint(s Service) Endpoint {
	return Endpoint{
		GetAllUsers: makeGetAllUsers(s),
		GetUser:     makeGetUser(s),
		CreateUser:  makeCreateUser(s),
		UpdateUser:  makeUpdateUser(s),
		DeleteUser:  makeDeleteUser(s),
	}
}

func makeGetAllUsers(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		users, err := s.GetAllUsers(ctx, req)
		if err != nil {
			return nil, err
		}
		return users, nil
	}
}

func makeGetUser(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		user, err := s.GetUser(ctx, req)
		if err != nil {
			return nil, err
		}
		return user, nil
	}
}

func makeCreateUser(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		res, err := s.CreateUser(ctx, req)
		if err != nil {
			return nil, err
		}
		return res, nil
	}
}

func makeUpdateUser(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		res, err := s.UpdateUser(ctx, req)
		if err != nil {
			return nil, err
		}
		return res, nil
	}
}

func makeDeleteUser(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		res, err := s.DeleteUser(ctx, req)
		if err != nil {
			return nil, err
		}
		return res, nil
	}
}
