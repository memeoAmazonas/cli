package auth

import (
	"cli-falabella/internal/security"
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoint struct {
	Login      endpoint.Endpoint
	LoginAdmin endpoint.Endpoint
	Logout     endpoint.Endpoint
}

func MakeEndpoint(s Service) Endpoint {
	return Endpoint{
		Login:      makeLogin(s),
		LoginAdmin: makeLoginAdmin(s),
		Logout:     makeLogout(s),
	}
}

func makeLogin(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(AuthRequest)
		res, err := s.Login(ctx, req)
		if err != nil {
			return nil, err
		}

		return res, nil
	}
}

func makeLoginAdmin(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(AuthAdminRequest)
		res, err := s.LoginAdmin(ctx, req)
		if err != nil {
			return nil, err
		}

		return res, nil
	}
}

func makeLogout(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*security.JwtClaim)
		res, err := s.Logout(ctx, *req)
		if err != nil {
			return nil, err
		}

		return res, nil
	}
}
