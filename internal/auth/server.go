package auth

import (
	"cli-falabella/internal/users"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/mongo"
)

func Server(db *mongo.Database, logger log.Logger) Endpoint {
	{
		logger = log.With(logger,
			"service", "User",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}
	level.Info(logger).Log("message", "auth service started")
	var srv Service
	{
		repository := users.NewRepository(db, logger)
		srv = NewService(repository, logger)
	}
	return MakeEndpoint(srv)
}
