package auth

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/security"
	"context"
	"encoding/json"
	"github.com/go-playground/validator"
	"net/http"
)

type AuthRequest struct {
	Token string `json:"token" validate:"required" example:"token"`
}

type AuthAdminRequest struct {
	User     string `json:"user" validate:"required" example:"usuario"`
	Password string `json:"password" validate:"required" example:"password"`
}

type AuthResponse struct {
	Id    string `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Token string `json:"token"`
}

func DecodeAuthRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req AuthRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}

	// validation
	validate := validator.New()
	err := validate.Struct(req)
	if err != nil {
		return nil, entity.ErrValidation
	}

	return req, nil
}

func DecodeAuthAdminRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req AuthAdminRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}

	// validation
	validate := validator.New()
	err := validate.Struct(req)
	if err != nil {
		return nil, entity.ErrValidation
	}

	return req, nil
}

func DecodeRequestLogout(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	return claims, nil
}
