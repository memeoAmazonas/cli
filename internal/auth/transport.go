package auth

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/middleware"
	"context"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"net/http"
)

func NewHttpServer(_ context.Context, endpoint Endpoint, logger log.Logger) http.Handler {
	opts := []httptransport.ServerOption{
		httptransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		httptransport.ServerErrorEncoder(entity.EncodeError),
	}

	r := mux.NewRouter()
	userRouter := r.PathPrefix("/api/auth").Subrouter()
	userRouter.Methods("POST").Path("/login").Handler(httptransport.NewServer(
		endpoint.Login,
		DecodeAuthRequest,
		entity.EncodeResponse,
		opts...,
	))

	userRouter.Methods("POST").Path("/admin").Handler(httptransport.NewServer(
		endpoint.LoginAdmin,
		DecodeAuthAdminRequest,
		entity.EncodeResponse,
		opts...,
	))

	userRouter.Methods("POST").Path("/logout").Handler(httptransport.NewServer(
		endpoint.Logout,
		DecodeRequestLogout,
		entity.EncodeResponse,
		opts...,
	))

	cors.Default().Handler(userRouter)
	return middleware.HeaderMiddleware(userRouter)
}
