package auth

import (
	"archive/zip"
	"cli-falabella/internal/entity"
	"cli-falabella/internal/helpers/cron"
	"cli-falabella/internal/helpers/git"
	"cli-falabella/internal/helpers/password"
	"cli-falabella/internal/security"
	"cli-falabella/internal/users"
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type Service interface {
	Login(ctx context.Context, auth AuthRequest) (*AuthResponse, error)
	LoginAdmin(ctx context.Context, auth AuthAdminRequest) (*AuthResponse, error)
	Logout(ctx context.Context, claims security.JwtClaim) (*entity.ResponseHttp, error)
}

type service struct {
	repository users.Repository
	logger     log.Logger
}

func NewService(repository users.Repository, logger log.Logger) *service {
	return &service{
		repository: repository,
		logger:     logger,
	}
}

// Login godoc
// @Summary Registration via token
// @Description User registration via token.
// @Tags Authentication
// @Accept  json
// @Produce  json
// @Param login body AuthRequest true "Auth GitLab"
// @Success 200 {object} AuthResponse
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Router /auth/login [post]
func (s service) Login(_ context.Context, auth AuthRequest) (*AuthResponse, error) {
	logger := log.With(s.logger, "service", "login")

	clientGit, users, err := git.AuthGitLab(auth.Token)
	if err != nil {
		level.Error(logger).Log("login", "error", "msg", err.Error())
		return nil, entity.ErrAuth
	}
	level.Info(logger).Log("login", "ok", "auth", users.Username)

	// generacion del token
	token, err := security.GenerateToken(strconv.Itoa(users.ID), users.Username, users.Name, users.Email, entity.RolUsers, auth.Token)
	if err != nil {
		level.Error(logger).Log("login", "error", "token", err.Error())
		return nil, entity.ErrAuth
	}
	level.Info(logger).Log("login", "ok", "token", "generate")

	// eliminando la ruta del usuario
	err = os.RemoveAll(fmt.Sprintf("template/%s", users.Username))
	if err != nil {
		level.Error(logger).Log("login", "error", "delete_folder", err.Error())
		return nil, entity.ErrFolderDelete
	}
	level.Info(logger).Log("login", "ok", "delete_folder", fmt.Sprintf("template/%s", users.Username))

	// verificando acceso grupos api integracion
	_, _, err = git.SearchGroupRouter(clientGit, viper.GetStringSlice("GITLAB_GROUPS_INTEGRATION"))
	if err != nil {
		level.Error(logger).Log("login", "error", "group", err.Error())
		return nil, entity.ErrGroupNotFound
	}
	// verificando acceso grupos api tecnicas
	_, _, err = git.SearchGroupRouter(clientGit, viper.GetStringSlice("GITLAB_GROUPS_TECHNICAL"))
	if err != nil {
		level.Error(logger).Log("login", "error", "group", err.Error())
		return nil, entity.ErrGroupNotFound
	}
	level.Info(logger).Log("login", "ok", "group", "ok")

	// obteniendo la plantilla
	projects, err := git.ListSearchProject(clientGit, viper.GetString("PROJECT_TOOL"))
	if err != nil {
		level.Error(logger).Log("login", "ok", viper.GetString("PROJECT_TOOL"), err.Error())
		return nil, entity.ErrTemplateOpen
	}
	if len(projects) == 0 {
		level.Error(logger).Log("login", "ok", viper.GetString("PROJECT_TOOL"), "project not fount")
		return nil, entity.ErrTemplateOpen
	}
	level.Info(logger).Log("login", "ok", viper.GetString("PROJECT_TOOL"), "ok")

	// creando carpeta del usuario
	_, err = os.Stat(fmt.Sprintf("template/%s", users.Username))
	if os.IsNotExist(err) {
		err = os.MkdirAll(fmt.Sprintf("template/%s", users.Username), 0755)
		if err != nil {
			level.Error(logger).Log("login", "ok", "folder", err.Error())
			return nil, entity.ErrFolderCreate
		}
	}
	level.Info(logger).Log("login", "ok", "folder", "ok")

	// descargando template
	archive, _, err := clientGit.Repositories.Archive(projects[0].PathWithNamespace, &gitlab.ArchiveOptions{
		Format: gitlab.String("zip"),
	})
	err = ioutil.WriteFile(fmt.Sprintf("template/%s/repositorio.zip", users.Username), archive, 0755)
	if err != nil {
		level.Error(logger).Log("login", "ok", "template_download", err.Error())
		return nil, entity.ErrTemplateDownload
	}
	level.Info(logger).Log("login", "ok", "template_download", "ok")

	// abriendo archivo template
	var filenames []string
	r, err := zip.OpenReader(fmt.Sprintf("template/%s/repositorio.zip", users.Username))
	if err != nil {
		level.Error(logger).Log("login", "ok", "template_open", err.Error())
		return nil, entity.ErrTemplateOpen
	}
	defer r.Close()
	for _, f := range r.File {
		fpath := filepath.Join(fmt.Sprintf("template/%s", users.Username), f.Name)
		if !strings.HasPrefix(fpath, filepath.Clean(fmt.Sprintf("template/%s", users.Username))+string(os.PathSeparator)) {
			level.Error(logger).Log("login", "ok", "template_open", err.Error())
			return nil, entity.ErrTemplateOpen
		}
		filenames = append(filenames, fpath)
		if f.FileInfo().IsDir() {
			os.MkdirAll(fpath, os.ModePerm)
			continue
		}
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			level.Error(logger).Log("login", "ok", "template_open", err.Error())
			return nil, entity.ErrTemplateOpen
		}
		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			level.Error(logger).Log("login", "ok", "template_open", err.Error())
			return nil, entity.ErrTemplateOpen
		}
		rc, err := f.Open()
		if err != nil {
			level.Error(logger).Log("login", "ok", "template_open", err.Error())
			return nil, entity.ErrTemplateOpen
		}
		_, err = io.Copy(outFile, rc)
		outFile.Close()
		rc.Close()
		if err != nil {
			level.Error(logger).Log("login", "ok", "template_open", err.Error())
			return nil, entity.ErrTemplateOpen
		}
	}
	if len(filenames) > 0 {
		os.Rename(filenames[0], fmt.Sprintf("template/%s/%s", users.Username, viper.GetString("PROJECT_TOOL")))
	}

	go cron.CronDelete(fmt.Sprintf("template/%s", users.Username), logger)

	level.Info(logger).Log("login", "ok", "logged", users.Username)
	return &AuthResponse{
		Id:    strconv.Itoa(users.ID),
		Name:  users.Name,
		Email: users.Email,
		Token: token,
	}, nil
}

// LoginAdmin godoc
// @Summary Registration via users
// @Description User registration via users.
// @Tags Authentication
// @Accept  json
// @Produce  json
// @Param login body AuthAdminRequest true "Auth API Rest"
// @Success 200 {object} AuthResponse
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Router /auth/admin [post]
func (s service) LoginAdmin(ctx context.Context, auth AuthAdminRequest) (*AuthResponse, error) {
	logger := log.With(s.logger, "service", "login admin")

	dataUsers, err := s.repository.GetAllUsers(ctx)
	if err != nil {
		level.Error(logger).Log("get users", err)
		return nil, entity.ErrRepositoryUsers
	}
	// creando el usuario administrador
	if len(dataUsers) == 0 {
		pass, _ := password.HashPassword("admin")

		user := users.User{
			ID:       primitive.NewObjectID(),
			Username: "admin",
			Password: pass,
			Name:     "Administrador CLI",
			Active:   true,
		}

		err := s.repository.CreateUser(ctx, user)
		if err != nil {
			level.Error(logger).Log("create auth", err.Error())
			return nil, entity.ErrRepositoryCreateUsers
		}
		level.Info(logger).Log("create auth", "ok")
	}

	countUser, err := s.repository.CountUserName(ctx, auth.User)
	if err != nil {
		level.Error(logger).Log("auth", "not found", "error", err.Error())
		return nil, entity.ErrRepositoryUsers
	}
	if *countUser == 0 {
		level.Warn(logger).Log("auth", "not found", "auth", auth.User)
		return nil, entity.ErrRepositoryUsersNotFound
	}

	dataUser, err := s.repository.GetUserName(ctx, auth.User)
	if err != nil {
		level.Error(logger).Log("auth", "error", err.Error())
		return nil, entity.ErrRepositoryUsers
	}
	// comprobando la contraseña
	if password.CheckPasswordHash(auth.Password, dataUser.Password) == false {
		level.Warn(logger).Log("password", "no match")
		return nil, entity.ErrPassword
	}

	// generacion del token
	token, err := security.GenerateToken(dataUser.ID.String(), auth.User, dataUser.Name, "", entity.RolAdmin, "")
	if err != nil {
		level.Error(logger).Log("login", "error", "token", err.Error())
		return nil, entity.ErrAuth
	}
	level.Info(logger).Log("login", "ok", "token", "generate")

	// actualizando datos usuario
	err = s.repository.UpdateLastAccess(ctx, auth.User)
	if err != nil {
		level.Error(logger).Log("update", err.Error())
		return nil, entity.ErrRepositoryCreateUsers
	}

	level.Info(logger).Log("login", "ok", "logged", auth.User)
	return &AuthResponse{
		Id:    dataUser.ID.Hex(),
		Name:  dataUser.Name,
		Email: "",
		Token: token,
	}, nil
}

// Logout godoc
// @Summary Logout app
// @Description Logout app.
// @Tags Authentication
// @Accept  json
// @Produce  json
// @Success 200 {object} entity.ResponseHttp
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /auth/logout [post]
func (s service) Logout(_ context.Context, claims security.JwtClaim) (*entity.ResponseHttp, error) {
	logger := log.With(s.logger, "service", "logout")

	_, users, err := git.AuthGitLab(claims.Token)
	if err != nil {
		level.Error(logger).Log("login", "error", "msg", err.Error())
		return nil, entity.ErrAuth
	}
	level.Info(logger).Log("login", "ok", "auth", users.Username)

	// eliminando la ruta del usuario
	err = os.RemoveAll(fmt.Sprintf("template/%s", users.Username))
	if err != nil {
		level.Error(logger).Log("login", "error", "delete_folder", err.Error())
		return nil, entity.ErrFolderDelete
	}
	level.Info(logger).Log("login", "ok", "delete_folder", fmt.Sprintf("template/%s", users.Username))

	return &entity.ResponseHttp{Code: 200, Message: "Logout"}, nil
}
