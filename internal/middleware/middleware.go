package middleware

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/security"
	"encoding/json"
	"net/http"
)

func HeaderMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

		next.ServeHTTP(w, r)
	})
}

func ProtectedUsers(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		token := request.Header.Get("Authorization")
		if len(token) > 0 {
			claims, err := security.Protected(token)
			if err != nil {
				json.NewEncoder(writer).Encode(map[string]interface{}{
					"code":  http.StatusUnauthorized,
					"key":   "AUTH_ERROR",
					"error": "Token error",
				})
			} else {
				if claims.Rol != entity.RolUsers {
					json.NewEncoder(writer).Encode(map[string]interface{}{
						"code":  http.StatusUnauthorized,
						"key":   "AUTH_ERROR",
						"error": "Not access",
					})
				} else {
					next.ServeHTTP(writer, request)
				}
			}
		} else {
			json.NewEncoder(writer).Encode(map[string]interface{}{
				"code":  http.StatusUnauthorized,
				"key":   "AUTH_ERROR",
				"error": "Token error",
			})
		}
	})
}

func ProtectedAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		token := request.Header.Get("Authorization")
		if len(token) > 0 {
			claims, err := security.Protected(token)
			if err != nil {
				json.NewEncoder(writer).Encode(map[string]interface{}{
					"code":  http.StatusUnauthorized,
					"key":   "AUTH_ERROR",
					"error": "Token error",
				})
			} else {
				if claims.Rol != entity.RolAdmin {
					json.NewEncoder(writer).Encode(map[string]interface{}{
						"code":  http.StatusUnauthorized,
						"key":   "AUTH_ERROR",
						"error": "Not access",
					})
				} else {
					next.ServeHTTP(writer, request)
				}
			}
		} else {
			json.NewEncoder(writer).Encode(map[string]interface{}{
				"code":  http.StatusUnauthorized,
				"key":   "AUTH_ERROR",
				"error": "Token error",
			})
		}
	})
}
