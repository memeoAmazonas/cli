package security

import (
	"errors"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
)

type JwtClaim struct {
	Id       string
	Username string
	Name     string
	Email    string
	Rol      string
	Token    string
	jwt.StandardClaims
}

func GenerateToken(id string, username, name, email, rol, tokenGitlab string) (string, error) {
	claims := &JwtClaim{
		Id:       id,
		Username: username,
		Name:     name,
		Email:    email,
		Rol:      rol,
		Token:    tokenGitlab,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Local().Add(time.Hour*time.Duration(viper.GetInt("JWT_EXP_HOUR")) + time.Minute*time.Duration(viper.GetInt("JWT_EXP_MIN")) + time.Second*time.Duration(viper.GetInt("JWT_EXP_SEC"))).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signedToken, err := token.SignedString([]byte(viper.GetString("JWT_SECRET")))
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func ParserToken(tk string) (*jwt.Token, error) {
	var (
		token *jwt.Token
		err   error
	)
	token, err = parseHS256(tk, token)
	if err != nil && err.Error() != "Token is expired" {
		token, err = parseHS256(tk, token)
	}
	return token, err
}

func parseHS256(tk string, token *jwt.Token) (*jwt.Token, error) {
	token, err := jwt.Parse(tk, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(viper.GetString("JWT_SECRET")), nil
	})
	return token, err
}

func Protected(signedToken string) (*JwtClaim, error) {
	token, err := jwt.ParseWithClaims(
		signedToken,
		&JwtClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(viper.GetString("JWT_SECRET")), nil
		},
	)
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*JwtClaim)
	if !ok {
		err = errors.New("Couldn't parse claims")
		return nil, err
	}

	if claims.ExpiresAt < time.Now().Local().Unix() {
		err = errors.New("JWT is expired")
		return nil, err
	}

	return claims, nil
}
