package database

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"os"
	"time"
)

func GetDatabase() (*mongo.Database, error) {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"database", "dbconnect",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}
	uri := fmt.Sprintf("mongodb://%s:%s/", viper.GetString("DB_HOST"), viper.GetString("DB_PORT"))
	fmt.Println(uri)
	dbName := viper.GetString("DB_NAME")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	err = client.Ping(context.Background(), readpref.Primary())
	if err != nil {
		level.Error(logger).Log("dbconnect", err)
		os.Exit(-1)
		return nil, err
	}
	level.Info(logger).Log("message", "Connection succesful")
	return client.Database(dbName), nil
}
