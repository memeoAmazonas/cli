package groups

import (
	"cli-falabella/internal/entity"
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoint struct {
	GetListGroup endpoint.Endpoint
}

func MakeEndpoint(s Service) Endpoint {
	return Endpoint{
		GetListGroup: makeGetListGroup(s),
	}
}

func makeGetListGroup(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		groups, err := s.GetListGroup(ctx, req)
		if err != nil {
			return nil, err
		}
		return groups, nil
	}
}
