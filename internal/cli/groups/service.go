package groups

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/helpers/git"
	"cli-falabella/internal/security"
	"context"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"strings"
)

type Service interface {
	GetListGroup(ctx context.Context, res entity.RequestHttp) ([]GroupResponse, error)
}

type service struct {
	repository *mongo.Database
	logger     log.Logger
}

func NewService(repository *mongo.Database, logger log.Logger) *service {
	return &service{
		repository: repository,
		logger:     logger,
	}
}

// GetListGroup godoc
// @Summary Returns a list of groups
// @Description Returns a list of groups.
// @Description Type API [technical, integration]
// @Tags Groups
// @Accept  json
// @Produce  json
// @Param   type      path   string     true  "Type API"	Enums(technical, integration)
// @Success 200 {array} GroupResponse
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /cli/groups/{type} [get]
func (s service) GetListGroup(_ context.Context, res entity.RequestHttp) ([]GroupResponse, error) {
	logger := log.With(s.logger, "cli", "list group")

	claims := res.Claims.(*security.JwtClaim)
	typeAPI := res.Params.(string)

	// auth gitlab
	clientGit, _, err := git.AuthGitLab(claims.Token)
	if err != nil {
		level.Error(logger).Log("group", "list", "auth", err.Error())
		return nil, entity.ErrAuth
	}

	var router string
	if typeAPI == "technique" {
		router, _, err = git.SearchGroupRouter(clientGit, viper.GetStringSlice("GITLAB_GROUPS_TECHNICAL"))
	} else {
		router, _, err = git.SearchGroupRouter(clientGit, viper.GetStringSlice("GITLAB_GROUPS_INTEGRATION"))
	}
	if err != nil {
		level.Error(logger).Log("group", "list", "list", err.Error())
		return nil, entity.ErrGroupNotFound
	}
	level.Info(logger).Log("groups", "list", "type", typeAPI)

	var responseGroup []GroupResponse
	responseGroup = []GroupResponse{}

	groups, err := git.ListSearchGroups(clientGit, "", false)
	if err != nil {
		level.Error(logger).Log("group", "list", "list", err.Error())
		return nil, entity.ErrGroupNotFound
	}
	for i := 0; i < len(groups); i++ {
		if strings.Contains(groups[i].FullPath, router+"/") {
			tempResponse := GroupResponse{
				Id:     groups[i].ID,
				Name:   groups[i].Name,
				Path:   strings.ReplaceAll(groups[i].FullPath, router+"/", ""),
				Parent: groups[i].ParentID,
			}
			if strings.Contains(tempResponse.Path, "/") == false {
				tempResponse.Parent = -1
			}
			responseGroup = append(responseGroup, tempResponse)
		}
	}

	return responseGroup, nil
}
