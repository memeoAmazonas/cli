package groups

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/security"
	"context"
	"github.com/gorilla/mux"
	"net/http"
)

type GroupResponse struct {
	Id     int    `json:"id" validate:"required" example:"23142"`
	Name   string `json:"name" validate:"required" example:"Group CLI"`
	Path   string `json:"path" validate:"required" example:"falabella/proyect"`
	Parent int    `json:"parent,omitempty" example:"23142"`
}

type GroupCreate struct {
	Root        string `json:"root" validate:"required" example:"group_cli"`
	Description string `json:"description" validate:"required" example:"Text example"`
	Type        string `json:"type" validate:"required" example:"integration"`
	Parent      int    `json:"parent,omitempty" example:"-1"`
}

type GroupUpdate struct {
	Name        string `json:"name" validate:"required" example:"Name group"`
	Description string `json:"description" validate:"required" example:"Text example"`
}

func DecodeRequestGroupList(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	vars := mux.Vars(r)

	return entity.RequestHttp{Claims: claims, Params: vars["type"]}, nil
}
