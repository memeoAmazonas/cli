package config

import (
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/mongo"
)

func Server(db *mongo.Database, logger log.Logger) Endpoint {
	{
		logger = log.With(
			logger,
			"service", "config",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}
	level.Info(logger).Log("message", "cli config service started")
	var srv Service
	{
		srv = NewService(db, logger)
	}
	return MakeEndpoint(srv)
}
