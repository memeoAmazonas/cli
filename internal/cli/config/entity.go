package config

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/security"
	"context"
	"encoding/json"
	"net/http"
)

type ResponseConfig struct {
	BranchMaster      string   `json:"branch_master,omitempty" validate:"required" example:"master"`
	BranchDevelop     string   `json:"branch_develop,omitempty" validate:"required" example:"develop"`
	Tags              []string `json:"tags,omitempty" validate:"required" example:"tag1,tag2,tag3"`
	MessageCommit     string   `json:"message_commit,omitempty" validate:"required" example:"Message commit gitlab"`
	UrlGitlab         string   `json:"url_gitlab,omitempty" validate:"required" example:"gitlab.com"`
	UrlApiIntegration string   `json:"url_api_integration,omitempty" validate:"required" example:"cl.gitlab"`
	UrlApiTechnique   string   `json:"url_api_technique,omitempty" validate:"required" example:"cl.gitlab"`
	ProjectToolName   string   `json:"project_tool_name,omitempty" validate:"required" example:"project-tool"`
}

func DecodeRequestGetConfig(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	return entity.RequestHttp{Claims: claims}, nil
}

func DecodeRequestUpdateConfig(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	var req ResponseConfig
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, entity.ErrValidation
	}

	return entity.RequestHttp{Claims: claims, Request: req}, nil
}
