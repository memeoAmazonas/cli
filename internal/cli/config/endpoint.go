package config

import (
	"cli-falabella/internal/entity"
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoint struct {
	GetConfig    endpoint.Endpoint
	UpdateConfig endpoint.Endpoint
}

func MakeEndpoint(s Service) Endpoint {
	return Endpoint{
		GetConfig:    makeGetConfig(s),
		UpdateConfig: makeUpdateConfig(s),
	}
}

func makeGetConfig(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		groups, err := s.GetConfig(ctx, req)
		if err != nil {
			return nil, err
		}
		return groups, nil
	}
}

func makeUpdateConfig(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		res, err := s.UpdateConfig(ctx, req)
		if err != nil {
			return nil, err
		}
		return res, nil
	}
}
