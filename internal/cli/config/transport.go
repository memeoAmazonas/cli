package config

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/middleware"
	"context"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"net/http"
)

func NewHttpServer(_ context.Context, endpoint Endpoint, logger log.Logger) http.Handler {
	opts := []httptransport.ServerOption{
		httptransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		httptransport.ServerErrorEncoder(entity.EncodeError),
	}

	r := mux.NewRouter()
	cliRouter := r.PathPrefix("/api/cli/config/").Subrouter()

	cliRouter.Methods("GET").Path("/").Handler(middleware.ProtectedAdmin(httptransport.NewServer(
		endpoint.GetConfig,
		DecodeRequestGetConfig,
		entity.EncodeResponse,
		opts...,
	)))

	cliRouter.Methods("PATCH").Path("/").Handler(middleware.ProtectedAdmin(httptransport.NewServer(
		endpoint.UpdateConfig,
		DecodeRequestUpdateConfig,
		entity.EncodeResponse,
		opts...,
	)))

	cors.Default().Handler(cliRouter)
	return middleware.HeaderMiddleware(cliRouter)
}
