package config

import (
	"cli-falabella/internal/entity"
	"context"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"os"
	"strings"
)

type Service interface {
	GetConfig(ctx context.Context, res entity.RequestHttp) (*ResponseConfig, error)
	UpdateConfig(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error)
}

type service struct {
	repository *mongo.Database
	logger     log.Logger
}

func NewService(repository *mongo.Database, logger log.Logger) *service {
	return &service{
		repository: repository,
		logger:     logger,
	}
}

// GetConfig godoc
// @Summary Returns the global configuration of the service
// @Description Returns the global configuration of the service. To confirm the changes you must restart manual service.
// @Tags Setting
// @Accept  json
// @Produce  json
// @Success 200 {object} ResponseConfig
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /cli/config [get]
func (s service) GetConfig(_ context.Context, res entity.RequestHttp) (*ResponseConfig, error) {
	apiIntegration := viper.GetStringSlice("GITLAB_GROUPS_INTEGRATION")
	var urlIntegration string
	for index := 0; index < len(apiIntegration); index++ {
		urlIntegration = urlIntegration + apiIntegration[index]
		if index != len(apiIntegration)-1 {
			urlIntegration = urlIntegration + "."
		}
	}

	apiTec := viper.GetStringSlice("GITLAB_GROUPS_TECHNICAL")
	var urlTec string
	for index := 0; index < len(apiTec); index++ {
		urlTec = urlTec + apiTec[index]
		if index != len(apiTec)-1 {
			urlTec = urlTec + "."
		}
	}

	config := &ResponseConfig{
		BranchMaster:      viper.GetString("GITLAB_BRANCH_MASTER"),
		BranchDevelop:     viper.GetString("GITLAB_BRANCH_DEVELOP"),
		Tags:              viper.GetStringSlice("GITLAB_TAG"),
		MessageCommit:     viper.GetString("GITLAB_MSG_COMMIT"),
		UrlGitlab:         viper.GetString("URL_GITLAB_BASE"),
		UrlApiIntegration: urlIntegration,
		UrlApiTechnique:   urlTec,
		ProjectToolName:   viper.GetString("PROJECT_TOOL"),
	}

	return config, nil
}

// UpdateConfig godoc
// @Summary Returns a list of groups
// @Description Returns a list of groups.
// @Tags Setting
// @Accept  json
// @Produce  json
// @Param config body ResponseConfig true "Config"
// @Success 200 {object} ResponseConfig
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /cli/config [patch]
func (s service) UpdateConfig(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error) {
	logger := log.With(s.logger, "update", "config")

	request := res.Request.(ResponseConfig)

	viper.Set("GITLAB_BRANCH_MASTER", request.BranchMaster)
	viper.Set("GITLAB_BRANCH_DEVELOP", request.BranchDevelop)
	viper.Set("GITLAB_TAG", request.Tags)
	viper.Set("GITLAB_MSG_COMMIT", request.MessageCommit)
	viper.Set("URL_GITLAB_BASE", request.UrlGitlab)
	viper.Set("GITLAB_GROUPS_INTEGRATION", strings.Split(request.UrlApiIntegration, "."))
	viper.Set("GITLAB_GROUPS_TECHNICAL", strings.Split(request.UrlApiTechnique, "."))
	viper.Set("PROJECT_TOOL", request.ProjectToolName)

	err := viper.WriteConfig()
	if err != nil {
		level.Error(logger).Log("error", err.Error())
		return nil, entity.ErrConfigSave
	}

	// recargar la configuracion
	viper.SetConfigFile("config.json")
	err = viper.ReadInConfig()
	if err != nil {
		level.Error(logger).Log("exit", err)
		os.Exit(-1)
	}

	level.Info(logger).Log("status", "ok")
	return &entity.ResponseHttp{
		Code:    200,
		Key:     "CONFIG_UPDATE",
		Message: "Configuration update",
		Data:    request,
	}, nil
}
