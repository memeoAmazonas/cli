package repositories

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/security"
	"context"
	"encoding/json"
	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"net/http"
)

type Template struct {
	Type      string `json:"type" example:"technical"`
	Language  string `json:"language" example:"java"`
	Framework string `json:"framework" example:"springboot"`
	Project   int    `json:"project" example:"123456"`
	Url       string `json:"url" example:"https://gitlab.com"`
}

type TemplateTechnicalCreate struct {
	IDProject       int    `json:"id_project" validate:"required" example:"12345"`
	PackageName     string `json:"-"`
	Core            string `json:"core" validate:"required" example:"core"`
	Product         string `json:"product" validate:"required" example:"product"`
	Language        string `json:"language" validate:"required" example:"java"`
	VersionLanguage string `json:"version_language" validate:"required" example:"16"`
	Framework       string `json:"framework" validate:"required" example:"springboot"`
	Feature         string `json:"feature" validate:"required" example:"test"`
	Version         string `json:"version" validate:"required" example:"1.0.1"`
	Compilation     string `json:"compilation" validate:"required" example:"gradle"`
	WsdlFileName    string `json:"wsdl_file_name" example:"wsdl"`
	HarborSubGroup  string `json:"harbor_sub_group" example:"harbor"`
}

type TemplateIntegrationCreate struct {
	IDProject       int    `json:"id_project" validate:"required" example:"12345"`
	Country         string `json:"country" validate:"required" example:"cl"`
	Scope           string `json:"scope" validate:"required" example:"banking"`
	Group           int    `json:"group" validate:"required" example:"12345"`
	Domain          string `json:"domain" validate:"required" example:"ESUP"`
	Capacity        string `json:"capacity" validate:"required" example:"document-management"`
	Language        string `json:"language" validate:"required" example:"Java"`
	VersionLanguage string `json:"version_language" validate:"required" example:"1.8"`
	Framework       string `json:"framework" validate:"required" example:"Micronauta"`
	Feature         string `json:"feature" validate:"required" example:"aaaa"`
	Version         string `json:"version" validate:"required" example:"1"`
	PackageName     string `json:"-"`
}

func DecodeRequestTemplateList(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	return claims, nil
}

func DecodeRequestTemplate(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	vars := mux.Vars(r)
	var params []string
	params = append(params, vars["type"])

	return entity.RequestHttp{Claims: claims, Params: params}, nil
}

func DecodeRequestTemplateIntegrationCreate(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	var req TemplateIntegrationCreate
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, entity.ErrValidation
	}

	// validation
	validate := validator.New()
	err = validate.Struct(req)
	if err != nil {
		return nil, entity.ErrValidation
	}

	return entity.RequestHttp{Request: req, Claims: claims}, nil
}

func DecodeRequestTemplateTechnicalCreate(_ context.Context, r *http.Request) (interface{}, error) {
	token := r.Header.Get("Authorization")
	claims, err := security.Protected(token)
	if err != nil {
		return nil, entity.ErrAuth
	}

	var req TemplateTechnicalCreate
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, entity.ErrValidation
	}

	// validation
	validate := validator.New()
	err = validate.Struct(req)
	if err != nil {
		//validationErrors := err.(validator.ValidationErrors)
		return nil, entity.ErrValidation
	}

	return entity.RequestHttp{Request: req, Claims: claims}, nil
}
