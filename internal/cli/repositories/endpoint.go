package repositories

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/security"
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoint struct {
	GetConfig                 endpoint.Endpoint
	GetTemplateList           endpoint.Endpoint
	CreateTemplateTechnical   endpoint.Endpoint
	CreateTemplateIntegration endpoint.Endpoint
	//DownloadTemplate          endpoint.Endpoint
}

func MakeEndpoint(s Service) Endpoint {
	return Endpoint{
		GetConfig:                 makeConfigRepositories(s),
		GetTemplateList:           makeGetTemplateList(s),
		CreateTemplateTechnical:   makeCreateTemplateTechnical(s),
		CreateTemplateIntegration: makeCreateTemplateIntegration(s),
		//DownloadTemplate:          makeDownloadRepositories(s),
	}
}

func makeConfigRepositories(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*security.JwtClaim)
		templates, err := s.GetLists(ctx, *req)
		if err != nil {
			return nil, err
		}
		return templates, err
	}
}

func makeGetTemplateList(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		templates, err := s.GetListTemplate(ctx, req)
		if err != nil {
			return nil, err
		}
		return templates, err
	}
}

func makeCreateTemplateTechnical(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		res, err := s.CreateTemplatesTechnical(ctx, req)
		if err != nil {
			return nil, err
		}
		return res, err
	}
}

func makeCreateTemplateIntegration(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(entity.RequestHttp)
		res, err := s.CreateTemplateIntegration(ctx, req)
		if err != nil {
			return nil, err
		}
		return res, err
	}
}
