package repositories

import (
	"bytes"
	"cli-falabella/internal/entity"
	"cli-falabella/internal/middleware"
	"cli-falabella/internal/security"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

func NewHttpServer(_ context.Context, endpoint Endpoint, logger log.Logger) http.Handler {
	opts := []httptransport.ServerOption{
		httptransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		httptransport.ServerErrorEncoder(entity.EncodeError),
	}

	r := mux.NewRouter()
	cliRouter := r.PathPrefix("/api/cli/templates").Subrouter()

	cliRouter.Methods("GET").Path("/config").Handler(middleware.ProtectedUsers(httptransport.NewServer(
		endpoint.GetConfig,
		DecodeRequestTemplateList,
		entity.EncodeResponse,
		opts...,
	)))

	cliRouter.Methods("GET").Path("/list/{type:[a-z]+}").Handler(middleware.ProtectedUsers(httptransport.NewServer(
		endpoint.GetTemplateList,
		DecodeRequestTemplate,
		entity.EncodeResponse,
		opts...,
	)))

	cliRouter.Methods("POST").Path("/technical").Handler(middleware.ProtectedUsers(httptransport.NewServer(
		endpoint.CreateTemplateTechnical,
		DecodeRequestTemplateTechnicalCreate,
		entity.EncodeResponse,
		opts...,
	)))

	cliRouter.Methods("POST").Path("/integration").Handler(middleware.ProtectedUsers(httptransport.NewServer(
		endpoint.CreateTemplateIntegration,
		DecodeRequestTemplateIntegrationCreate,
		entity.EncodeResponse,
		opts...,
	)))

	cliRouter.Methods("GET").Path("/download").HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			token := r.Header.Get("Authorization")
			if len(token) > 0 {
				claims, err := security.Protected(token)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				} else {
					destiny := fmt.Sprintf("./template/%s/%s", claims.Username, "project.zip")
					_, err := os.Stat(destiny)
					if os.IsNotExist(err) {
						w.WriteHeader(400)
						json.NewEncoder(w).Encode(entity.ResponseHttp{Code: 400, Message: "File not found"})
						return
					}

					downloadBytes, _ := ioutil.ReadFile(destiny)
					mime := http.DetectContentType(downloadBytes)
					fileSize := len(string(downloadBytes))
					w.Header().Set("Content-Type", mime)
					w.Header().Set("Content-Disposition", "attachment; filename="+destiny+"")
					w.Header().Set("Expires", "0")
					w.Header().Set("Content-Transfer-Encoding", "binary")
					w.Header().Set("Content-Length", strconv.Itoa(fileSize))
					w.Header().Set("Content-Control", "private, no-transform, no-store, must-revalidate")
					http.ServeContent(w, r, destiny, time.Now(), bytes.NewReader(downloadBytes))

					return
				}
			}
		})

	cors.Default().Handler(cliRouter)
	return middleware.HeaderMiddleware(cliRouter)
}
