package repositories

import (
	"cli-falabella/internal/entity"
	"cli-falabella/internal/helpers/files"
	"cli-falabella/internal/helpers/git"
	"cli-falabella/internal/helpers/parseTemplate"
	"cli-falabella/internal/security"
	"context"
	"errors"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"os"
	"strings"
)

type Service interface {
	GetLists(ctx context.Context, claims security.JwtClaim) (map[string][]string, error)
	GetListTemplate(ctx context.Context, res entity.RequestHttp) (*[]Template, error)
	CreateTemplatesTechnical(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error)
	CreateTemplateIntegration(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error)
	DownloadTemplate(ctx context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error)
}

type service struct {
	repository *mongo.Database
	logger     log.Logger
}

func NewService(repository *mongo.Database, logger log.Logger) *service {
	return &service{
		repository: repository,
		logger:     logger,
	}
}

// GetLists godoc
// @Summary List config repository
// @Description List config repository.
// @Tags Templates
// @Accept  json
// @Produce  json
// @Success 200 {array} string
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /cli/templates/config [get]
func (s service) GetLists(_ context.Context, claims security.JwtClaim) (map[string][]string, error) {
	logger := log.With(s.logger, "service", "list config")
	base := fmt.Sprintf("template/%s/%s", claims.Username, viper.GetString("PROJECT_TOOL"))
	res, err := files.GetFromFile(base+"/repo-configuration/create-new-repo.sh", entity.GetKeyCliList())
	if err != nil {
		level.Error(logger).Log("list", err.Error())
		return nil, entity.ErrFileOpen
	}
	level.Info(logger).Log("list", "ok")
	return res, nil
}

// GetListTemplate godoc
// @Summary List template
// @Description List config repository.
// @Tags Templates
// @Accept  json
// @Produce  json
// @Param   type      path   string     true  "Type API" 	Enums(technical, integration)
// @Success 200 {array} Template
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /cli/templates/list/{type} [get]
func (s service) GetListTemplate(_ context.Context, res entity.RequestHttp) (*[]Template, error) {
	logger := log.With(s.logger, "service", "repositories", "list", "template")

	claims := res.Claims.(*security.JwtClaim)
	params := res.Params.([]string)

	// auth gitlab
	clientGit, user, err := git.AuthGitLab(claims.Token)
	if err != nil {
		level.Error(logger).Log("auth", err.Error())
		return nil, entity.ErrAuth
	}
	logger = log.With(s.logger, "users", user.Username)
	logger = log.With(s.logger, "type", params[0])

	template := "template-" + params[0] + "-"

	// list project
	projects, err := git.ListSearchProject(clientGit, template)
	if err != nil {
		level.Error(logger).Log("list project", err)
		return nil, entity.ErrProjectList
	}
	level.Info(logger).Log("list project", "ok")

	var data []Template
	data = []Template{}
	for i := 0; i < len(projects); i++ {
		if len(strings.Split(projects[i].Path, "-")) >= 4 {
			data = append(data, Template{
				Type:      params[0],
				Language:  strings.SplitN(projects[i].Path, "-", 4)[2],
				Framework: strings.SplitN(projects[i].Path, "-", 4)[3],
				Project:   projects[i].ID,
				Url:       projects[i].WebURL,
			})
		}
	}

	return &data, nil
}

// CreateTemplatesTechnical godoc
// @Summary Create a repository
// @Description Create a repository in the group marked by the auth.
// @Tags Templates
// @Accept  json
// @Produce  json
// @Param repositorie body TemplateTechnicalCreate true "Repositories create"
// @Success 201 {object} entity.ResponseHttp
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /cli/templates/technical [post]
func (s service) CreateTemplatesTechnical(_ context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error) {
	logger := log.With(s.logger, "service", "repositories", "create", "technical")

	claims := res.Claims.(*security.JwtClaim)
	request := res.Request.(TemplateTechnicalCreate)

	urlProject := fmt.Sprintf("template/%s/%s-temp", claims.Username, request.Feature)
	request.PackageName = "fif.bfa.cl." + request.Core + "." + request.Product

	// auth gitlab
	clientGit, user, err := git.AuthGitLab(claims.Token)
	if err != nil {
		level.Error(logger).Log("auth", err.Error())
		return nil, entity.ErrAuth
	}
	logger = log.With(s.logger, "users", user.Username)

	template := "template-technical-" + strings.ToLower(request.Language) + "-" + strings.ToLower(request.Framework)

	// delete folder template
	err = files.DeleteFolder(fmt.Sprintf("template/%s/%s-temp", claims.Username, template))
	if err != nil {
		level.Error(logger).Log("delete template", err.Error())
		return nil, entity.ErrFolderDelete
	}

	// list project
	projectTemplate, err := git.SearchProject(clientGit, request.IDProject)
	if err != nil {
		level.Error(logger).Log("list project", err)
		return nil, entity.ErrProjectList
	}

	// download project
	archiveRepoTemplate := fmt.Sprintf("template/%s/%s.zip", claims.Username, template)
	err = git.DownloadRepoArchivo(clientGit, projectTemplate.PathWithNamespace, archiveRepoTemplate)
	if err != nil {
		level.Error(logger).Log("download project", err)
		return nil, entity.ErrTemplateDownload
	}
	level.Info(logger).Log("download project", "ok", "dir", archiveRepoTemplate)

	// unzip project
	folderUsers := fmt.Sprintf("template/%s", claims.Username)
	err = files.UnZip(archiveRepoTemplate, folderUsers, urlProject)
	if err != nil {
		level.Error(logger).Log("unzip project", err)
		return nil, entity.ErrTemplateDownload
	}
	level.Info(logger).Log("unzip project", "ok")
	os.Remove(archiveRepoTemplate)

	// Refactizacion de las carpetas
	err = files.RefactoriceFolders(fmt.Sprintf("template/%s/%s-temp", claims.Username, request.Feature), request.Core, request.Product)
	if err != nil {
		level.Error(logger).Log("parse folder tmpl", err.Error())
		return nil, entity.ErrTmplFolder
	}
	level.Info(logger).Log("parse folder tmpl", "ok")

	// template repo
	listPath, err := files.ListinFiles(urlProject)
	if err != nil {
		level.Error(logger).Log("list file", err.Error())
		return nil, entity.ErrFolderList
	}
	for _, val := range listPath {
		err := parseTemplate.ParseTemplate(val, request, logger)
		if err != nil {
			os.RemoveAll(urlProject)
			level.Error(logger).Log("parse tmpl", err.Error())
			return nil, entity.ErrTmplFile
		}
	}
	level.Info(logger).Log("parse tmpl", "ok")

	// search group
	_, group, err := git.SearchGroupRouter(clientGit, viper.GetStringSlice("GITLAB_GROUPS_TECHNICAL"))
	if err != nil {
		level.Error(logger).Log("group search", err.Error())
		return nil, entity.ErrGroupNotFound
	}

	// commit gitlab
	project, err := git.CommitRepo(logger, clientGit, urlProject, fmt.Sprintf("template/%s/%s-temp", claims.Username, template), request.Feature, group.ID, request.Version)
	if err != nil {
		return nil, entity.ErrProjectCommit
	}
	level.Info(logger).Log("commit", "ok", "project", urlProject)

	// download repo to git
	err = git.DownloadRepo(fmt.Sprintf("template/%s/%s", claims.Username, request.Feature), claims.Token, strings.ReplaceAll(project.HTTPURLToRepo, "https://", ""))
	if err != nil {
		level.Error(logger).Log("download template project", err)
		return nil, entity.ErrTemplateDownload
	}

	// compress template
	err = files.Zipit(fmt.Sprintf("template/%s/%s", claims.Username, request.Feature), fmt.Sprintf("template/%s/%s.zip", claims.Username, "project"))
	if err != nil {
		level.Error(logger).Log("download template project", err)
		return nil, entity.ErrTemplateDownload
	}
	level.Info(logger).Log("download template project", "ok", "dir", urlProject)

	level.Info(logger).Log("template", "ok")
	return &entity.ResponseHttp{Code: 201, Key: "TEMPLATE_CREATE", Message: "Template created"}, nil
}

// CreateTemplateIntegration godoc
// @Summary Create a repository integration
// @Description Create a repository in the group marked by the auth.
// @Tags Templates
// @Accept  json
// @Produce  json
// @Param repositorie body TemplateIntegrationCreate true "Repositories create"
// @Success 201 {object} entity.ResponseHttp
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /cli/templates/integration [post]
func (s service) CreateTemplateIntegration(_ context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error) {
	logger := log.With(s.logger, "service", "repositories", "create", "integration")

	level.Info(logger).Log("template", "integration")

	claims := res.Claims.(*security.JwtClaim)
	request := res.Request.(TemplateIntegrationCreate)
	urlProject := fmt.Sprintf("template/%s/%s-temp", claims.Username, request.Feature)

	// auth gitlab
	clientGit, user, err := git.AuthGitLab(claims.Token)
	if err != nil {
		level.Error(logger).Log("auth", err.Error())
		return nil, entity.ErrAuth
	}
	logger = log.With(s.logger, "users", user.Username)

	template := "template-integration-" + strings.ToLower(request.Language) + "-" + strings.ToLower(request.Framework)

	// delete folder template
	err = files.DeleteFolder(fmt.Sprintf("template/%s/%s", claims.Username, template))
	if err != nil {
		level.Error(logger).Log("delete template", err.Error())
		return nil, entity.ErrFolderDelete
	}

	// limpiando estructura del proyecto
	err = files.DeleteFolder(urlProject)
	if err != nil {
		return nil, errors.New("Not deleted folder")
	}

	// list project
	projectTemplate, err := git.SearchProject(clientGit, request.IDProject)
	if err != nil {
		level.Error(logger).Log("list project", err)
		return nil, entity.ErrProjectList
	}
	level.Info(logger).Log("list project", "ok")

	// download project
	_ = os.MkdirAll("template/aclGitlab", os.ModePerm)
	archiveRepoTemplate := fmt.Sprintf("template/%s/%s.zip", claims.Username, template)
	err = git.DownloadRepoArchivo(clientGit, projectTemplate.PathWithNamespace, archiveRepoTemplate)
	if err != nil {
		level.Error(logger).Log("download project", err)
		return nil, entity.ErrTemplateDownload
	}
	level.Info(logger).Log("download project", "ok", "dir", archiveRepoTemplate)

	// unzip project
	folderUsers := fmt.Sprintf("template/%s", claims.Username)
	err = files.UnZip(archiveRepoTemplate, folderUsers, urlProject)
	if err != nil {
		level.Error(logger).Log("unzip project", err)
		return nil, entity.ErrTemplateDownload
	}
	level.Info(logger).Log("unzip project", "ok")
	os.Remove(archiveRepoTemplate)

	// Refactizacion de las carpetas
	err = files.RefactoriceFolders(urlProject, request.Domain, request.Capacity)
	if err != nil {
		level.Error(logger).Log("parse folder tmpl", err.Error())
		return nil, entity.ErrTmplFolder
	}
	level.Info(logger).Log("parse folder tmpl", "ok")

	// template repo
	listPath, err := files.ListinFiles(urlProject)
	if err != nil {
		level.Error(logger).Log("list file", err.Error())
		return nil, entity.ErrFolderList
	}

	featureApp := request.Feature
	for _, val := range listPath {
		packageName, _ := files.GetPackageName(request.Domain, request.Capacity)
		packageName = strings.ToLower(strings.ReplaceAll(packageName, "\\", "."))
		packageName = strings.ToLower(strings.ReplaceAll(packageName, "/", "."))
		request.PackageName = packageName

		err := parseTemplate.ParseTemplate(val, request, logger)
		if err != nil {
			os.RemoveAll(urlProject)
			level.Error(logger).Log("parse tmpl", err.Error())
			return nil, entity.ErrTmplFile
		}
	}
	level.Info(logger).Log("parse tmpl", "ok")
	request.Feature = featureApp

	// limpiando estructura del proyecto
	err = files.DeleteFolder(fmt.Sprintf("template/%s/%s-temp/src/main/java/NOMBRE_PACKAGE", claims.Username, request.Feature))
	if err != nil {
		return nil, errors.New("Not deleted folder")
	}

	// commit gitlab
	project, err := git.CommitRepo(logger, clientGit, urlProject, fmt.Sprintf("template/%s/%s", claims.Username, template), request.Feature, request.Group, request.Version)
	if err != nil {
		return nil, entity.ErrProjectCommit
	}
	level.Info(logger).Log("commit", "ok", "project", urlProject)

	// download repo to git
	err = git.DownloadRepo(fmt.Sprintf("template/%s/%s", claims.Username, request.Feature), claims.Token, strings.ReplaceAll(project.HTTPURLToRepo, "https://", ""))
	if err != nil {
		level.Error(logger).Log("download template project", err)
		return nil, entity.ErrTemplateDownload
	}

	// compress template
	err = files.Zipit(fmt.Sprintf("template/%s/%s", claims.Username, request.Feature), fmt.Sprintf("template/%s/%s.zip", claims.Username, "project"))
	if err != nil {
		level.Error(logger).Log("download template project", err)
		return nil, entity.ErrTemplateDownload
	}
	level.Info(logger).Log("download template project", "ok", "dir", urlProject)

	level.Info(logger).Log("template", "ok")
	return &entity.ResponseHttp{Code: 201, Key: "TEMPLATE_CREATE", Message: "Template created"}, nil
}

// DownloadTemplate godoc
// @Summary Download file template
// @Description Download file template.
// @Tags Templates
// @Accept  json
// @Produce  mpfd
// @Success 200 {file} string
// @Failure 400,401 {object} entity.ResponseHttp
// @Failure 404 {object} entity.ResponseHttp
// @Failure default {object} entity.ResponseHttp
// @Security ApiKeyAuth
// @Router /cli/templates/download [get]
func (s service) DownloadTemplate(_ context.Context, res entity.RequestHttp) (*entity.ResponseHttp, error) {
	logger := log.With(s.logger, "service", "template", "download")

	level.Info(logger).Log("template", "download")

	claims := res.Claims.(*security.JwtClaim)

	return &entity.ResponseHttp{Code: 200, Message: claims.Username}, nil
}
