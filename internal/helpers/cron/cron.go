package cron

import (
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/mileusna/crontab"
	"github.com/spf13/viper"
	"os"
)

func CronDelete(src string, logger log.Logger) {
	level.Info(logger).Log("cron", "start", "folder", src)
	cron := crontab.New()
	minutes := viper.GetString("CRON_MINUTES")
	hours := viper.GetString("CRON_HOURS")
	schedule := fmt.Sprintf("%s %s * * *", minutes, hours)
	cron.AddJob(schedule, func() {
		err := os.RemoveAll(src)
		if err != nil {
			level.Error(logger).Log("cron", "error", "delete", "error delete src "+src)
		} else {
			level.Error(logger).Log("cron", "ok", "delete", fmt.Sprintf("workspace %s delete success", src))
			cron.Shutdown()
		}
	})
}
