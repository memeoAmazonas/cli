package files

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

func ListinFiles(path string) ([]string, error) {
	var response []string
	err := filepath.Walk(path, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if strings.Contains(path, ".tmpl") {
			response = append(response, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return response, nil
}

func ListinFilesInDirectory(path string) ([]string, error) {
	var response []string
	err := filepath.Walk(path, func(path string, info fs.FileInfo, err error) error {
		response = append(response, path)
		return nil
	})
	if err != nil {
		return nil, err
	}
	return response, nil
}

func GetPackageName(domain string, capacity string) (string, error) {
	scope := "fif/bfa/cl"
	domain = strings.ReplaceAll(domain, ".", "/")
	capacity = strings.ReplaceAll(capacity, ".", "/")
	if domain == "" {
		return "", errors.New("No se ha especificado el dominio")
	}

	if capacity == "" {
		return "", errors.New("No se ha especificado la capacidad")
	}

	return filepath.Join(scope, domain, capacity), nil
}

func RefactoriceFolders(urlProject string, domain string, capacity string) error {
	packageName, err := GetPackageName(domain, capacity)
	if err != nil {
		return err
	}

	directories, _ := ListinFilesInDirectory(urlProject)

	for _, val := range directories {
		if strings.Contains(val, "NOMBRE_PACKAGE") {
			otherVal := strings.ReplaceAll(val, "NOMBRE_PACKAGE", packageName)

			fileInfo, _ := os.Stat(val)
			if fileInfo.IsDir() {
				err = os.MkdirAll(otherVal, os.ModePerm)
			} else {
				_, err = CopyFiles(val, otherVal)
			}

			if err != nil {
				return err
			}
		}

	}
	_ = filepath.Walk("template", func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if strings.Contains(path, "NOMBRE_PACKAGE") {
			err = DeleteFolder(path)
			if err != nil {
				return nil
			}
		}
		return nil
	})
	return nil
}

func CopyFiles(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer func(source *os.File) {
		_ = source.Close()
	}(source)

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer func(destination *os.File) {
		_ = destination.Close()
	}(destination)
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
