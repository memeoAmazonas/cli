package files

import (
	"errors"
	"fmt"
	"io/ioutil"
	"reflect"
	"strings"
)

func GetFromFile(path string, values []string) (map[string][]string, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.New("unable to open file" + path)
	}
	response := createMap(values, strings.Split(string(data), "\n"))
	keys := reflect.ValueOf(response).MapKeys()
	if len(values) > len(keys) {
		return nil, errors.New(fmt.Sprintf("unable to get value in %s", path))
	}
	return response, nil
}

func removeEmptyStrings(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}
func createMap(values, file []string) map[string][]string {
	response := make(map[string][]string)
	for _, actual := range values {
		for _, val := range file {
			if strings.Contains(val, actual) {
				result := strings.ReplaceAll(val, actual+"=(", "")
				result = strings.ReplaceAll(result, ")", "")
				result = strings.ReplaceAll(result, "\"", "")
				response[actual] = removeEmptyStrings(strings.Split(result, " "))
				break
			}
		}
	}
	return response
}
