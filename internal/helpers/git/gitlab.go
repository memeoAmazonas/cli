package git

import (
	"bufio"
	"cli-falabella/internal/helpers/files"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"io/fs"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func AuthGitLab(token string) (*gitlab.Client, *gitlab.User, error) {
	clientGit, err := gitlab.NewClient(token, gitlab.WithBaseURL(fmt.Sprintf("https://%s", viper.GetString("URL_GITLAB_BASE"))))
	if err != nil {
		return nil, nil, err
	}

	users, _, err := clientGit.Users.CurrentUser()
	if err != nil {
		return nil, nil, errors.New("Auth error")
	}

	return clientGit, users, nil
}

func SearchProject(clientGit *gitlab.Client, id int) (*gitlab.Project, error) {
	opt_list_project := gitlab.ListProjectsOptions{
		Membership: gitlab.Bool(true),
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 50,
		},
	}

	for {
		tempProjects, res, err := clientGit.Projects.ListProjects(&opt_list_project)
		if err != nil {
			return nil, err
		}

		for i := 0; i < len(tempProjects); i++ {
			if tempProjects[i].ID == id {
				return tempProjects[i], nil
			}
		}

		if res.CurrentPage >= res.TotalPages {
			break
		}

		opt_list_project.Page = res.NextPage
	}

	return nil, errors.New("Project not fount")
}

func ListSearchProject(clientGit *gitlab.Client, search string) ([]*gitlab.Project, error) {
	projects, _, err := clientGit.Projects.ListProjects(&gitlab.ListProjectsOptions{
		//Visibility: gitlab.Visibility(visibility),
		Search:     gitlab.String(search),
		Membership: gitlab.Bool(true),
	})
	if err != nil {
		return nil, err
	}

	return projects, nil
}

func DownloadRepoArchivo(clientGit *gitlab.Client, namespace string, pathFile string) error {
	archive, _, err := clientGit.Repositories.Archive(namespace, &gitlab.ArchiveOptions{
		Format: gitlab.String("zip"),
	})
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(pathFile, archive, 0755)
	if err != nil {
		os.Remove(pathFile)
		return err
	}

	return err
}

func ListGroups(clientGit *gitlab.Client, parent string) ([]*gitlab.Group, error) {
	groups, _, err := clientGit.Groups.ListGroups(&gitlab.ListGroupsOptions{
		AllAvailable: gitlab.Bool(true),
		Owned:        gitlab.Bool(true),
	})

	return groups, err
}

func ListSearchGroups(clientGit *gitlab.Client, search string, exact bool) (groups []*gitlab.Group, err error) {
	opt_list_group := gitlab.ListGroupsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 50,
		},
		Search: gitlab.String(search),
	}

	for {
		tempGroup, res, err := clientGit.Groups.ListGroups(&opt_list_group)
		if err != nil {
			return nil, err
		}
		for i := 0; i < len(tempGroup); i++ {
			if exact == true {
				if tempGroup[i].Path == search {
					groups = append(groups, tempGroup[i])
				}
			} else {
				groups = append(groups, tempGroup[i])
			}
		}

		if res.CurrentPage >= res.TotalPages {
			break
		}

		opt_list_group.Page = res.NextPage
	}

	if len(groups) == 0 {
		return nil, errors.New("not group")
	}

	return
}

func SearchGroupID(clientGit *gitlab.Client, idGroup int) (*gitlab.Group, error) {
	groups, err := ListSearchGroups(clientGit, "", false)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(groups); i++ {
		if groups[i].ID == idGroup {
			return groups[i], nil
		}
	}

	return nil, errors.New("group not found")
}

func SearchGroupRouter(clientGit *gitlab.Client, groupsEnv []string) (string, *gitlab.Group, error) {
	if len(groupsEnv) == 0 {
		return "", nil, errors.New("env not groups")
	}

	router := ""
	var parentGroup *gitlab.Group
	for i := 0; i < len(groupsEnv); i++ {
		router += groupsEnv[i]
		if i != len(groupsEnv)-1 {
			router += "/"
		}

		tempGroup, err := ListSearchGroups(clientGit, groupsEnv[i], true)
		if err != nil {
			return "", nil, err
		}
		// buscar que el primer elemento no tenga padre
		if i == 0 {
			if tempGroup[0].ParentID != 0 {
				return "", nil, errors.New("parent group incorrect")
			}
		} else {
			// comprobar la organizacion de la ruta
			if tempGroup[0].ParentID != parentGroup.ID {
				return "", nil, errors.New(fmt.Sprintf("parent incorrect %s", tempGroup[i].Name))
			}
		}
		parentGroup = tempGroup[0]
	}

	return router, parentGroup, nil
}

func CreateGroup(clientGit *gitlab.Client, name, description string, parent int) (*gitlab.Group, error) {
	group, _, err := clientGit.Groups.CreateGroup(&gitlab.CreateGroupOptions{
		Name:        gitlab.String(strings.ToLower(name)),
		Path:        gitlab.String(strings.ToLower(name)),
		Description: gitlab.String(description),
		Visibility:  gitlab.Visibility(gitlab.PrivateVisibility),
		ParentID:    gitlab.Int(parent),
	})

	return group, err
}

func UpdateGroup(clientGit *gitlab.Client, idGroup int, name, description string) (*gitlab.Group, error) {
	opt_group_update := &gitlab.UpdateGroupOptions{
		Name:        gitlab.String(name),
		Description: gitlab.String(name),
	}
	group, _, err := clientGit.Groups.UpdateGroup(idGroup, opt_group_update)
	if err != nil {
		return nil, errors.New("Not update groups")
	}

	return group, nil
}

func DeleteGroup(clientGit *gitlab.Client, group int) error {
	_, err := clientGit.Groups.DeleteGroup(group)

	return err
}

func CreateProject(clientGit *gitlab.Client, name string, group int) (*gitlab.Project, error) {
	project, _, err := clientGit.Projects.CreateProject(&gitlab.CreateProjectOptions{
		Name:                 gitlab.String(name),
		NamespaceID:          gitlab.Int(group),
		Description:          gitlab.String(viper.GetString("GITLAB_MSG_COMMIT")),
		Visibility:           gitlab.Visibility(gitlab.PrivateVisibility),
		MergeRequestsEnabled: gitlab.Bool(true),
		SnippetsEnabled:      gitlab.Bool(true),
	})
	if err != nil {
		return nil, errors.New("error create proyect")
	}

	return project, nil
}

func CommitRepo(logger log.Logger, clientGit *gitlab.Client, pathFolder string, pathTemplate string, feature string, idgroup int, version string) (*gitlab.Project, error) {
	level.Info(logger).Log("commit", "start")
	group, err := SearchGroupID(clientGit, idgroup)
	if err != nil {
		level.Error(logger).Log("group", err.Error())
		return nil, err
	}

	// validation project
	projects, err := ListSearchProject(clientGit, feature)
	if err != nil {
		files.DeleteFolder(pathFolder)
		level.Error(logger).Log("proyect", "error proyect")
		return nil, errors.New("proyect exits")
	}
	if len(projects) > 0 {
		var dataGroup = []string{}
		for index := 0; index < strings.Count(projects[0].PathWithNamespace, "/"); index++ {
			dataGroup = append(dataGroup, strings.Split(projects[0].PathWithNamespace, "/")[index])
		}
		_, groupProject, err := SearchGroupRouter(clientGit, dataGroup)
		if err != nil {
			return nil, err
		}

		if groupProject.ID == idgroup {
			if projects[0].Name == feature {
				files.DeleteFolder(pathFolder)
				level.Error(logger).Log("proyect", "proyect exits")
				return nil, errors.New("proyect exits")
			}
		}
	}

	// create project
	project, err := CreateProject(clientGit, feature, group.ID)
	if err != nil {
		level.Error(logger).Log("proyect", err.Error())
		return nil, err
	}
	level.Info(logger).Log("proyect", "ok")

	// commit repo
	files := []string{}
	err = filepath.Walk(pathFolder, func(path string, info fs.FileInfo, err error) error {
		fi, err := os.Stat(path)
		if err != nil {
			level.Error(logger).Log("file", err.Error())
			return err
		}

		if path != pathTemplate && fi.Mode().IsRegular() == true {
			path_temp := strings.Replace(pathFolder, "/", "\\", -1)
			files = append(files, strings.Replace(path, path_temp+"\\", "", 1))
		}
		return nil
	})
	opt_create_commit := gitlab.CreateCommitOptions{
		Branch:        gitlab.String(viper.GetString("GITLAB_BRANCH_MASTER")),
		CommitMessage: gitlab.String(viper.GetString("GITLAB_MSG_COMMIT")),
		StartBranch:   gitlab.String(viper.GetString("GITLAB_BRANCH_MASTER")),
		Actions:       nil,
		Force:         gitlab.Bool(true),
	}
	for _, file := range files {
		file_open, err := os.Open(pathFolder + "/" + strings.ReplaceAll(strings.Replace(file, "\\", "/", -1), pathFolder, ""))
		if err == nil {
			reader := bufio.NewReader(file_open)
			content, _ := ioutil.ReadAll(reader)
			action := gitlab.CommitActionOptions{
				Action:   gitlab.FileAction(gitlab.FileCreate),
				FilePath: gitlab.String(strings.Replace(strings.Replace(file, "\\", "/", -1), pathFolder, "", -1)),
				Content:  gitlab.String(base64.StdEncoding.EncodeToString(content)),
				Encoding: gitlab.String("base64"),
			}
			opt_create_commit.Actions = append(opt_create_commit.Actions, &action)
			file_open.Close()
		}
	}
	_, _, err = clientGit.Commits.CreateCommit(project.PathWithNamespace, &opt_create_commit)
	if err != nil {
		level.Error(logger).Log("commit", err.Error())
		clientGit.Projects.DeleteProject(project.PathWithNamespace)
		return nil, errors.New("error commit")
	}

	// create branch
	err = CreateBranch(clientGit, project.ID, viper.GetString("GITLAB_BRANCH_MASTER"), viper.GetString("GITLAB_BRANCH_DEVELOP"))
	if err != nil {
		level.Error(logger).Log("branch", err.Error())
		clientGit.Projects.DeleteProject(project.PathWithNamespace)
		return nil, errors.New("error branch")
	}
	level.Info(logger).Log("branch", "ok")

	// create label
	tags := viper.GetStringSlice("GITLAB_TAG")
	level.Info(logger).Log("label", len(tags))
	for i := 0; i < len(tags); i++ {
		err = CreateLabel(clientGit, project.ID, tags[i])
		if err != nil {
			level.Error(logger).Log(fmt.Sprintf("tag %s", tags[i]), err.Error())
			clientGit.Projects.DeleteProject(project.PathWithNamespace)
			return nil, errors.New(fmt.Sprintf("error tag %s", tags[i]))
		}
		level.Info(logger).Log(fmt.Sprintf("tag %s", tags[i]), "ok")
	}

	return project, nil
}

func CreateBranch(clientGit *gitlab.Client, project int, ref, branch string) error {
	_, _, err := clientGit.Branches.CreateBranch(project, &gitlab.CreateBranchOptions{
		Branch: gitlab.String(branch),
		Ref:    gitlab.String(ref),
	})
	if err != nil {
		return err
	}

	return nil
}

func CreateTags(clientGit *gitlab.Client, project int, branch, name string) error {
	_, _, err := clientGit.Tags.CreateTag(project, &gitlab.CreateTagOptions{
		TagName: gitlab.String(name),
		Ref:     gitlab.String(branch),
	})
	if err != nil {
		return err
	}

	return nil
}

func CreateLabel(clientGit *gitlab.Client, project int, name string) error {
	_, _, err := clientGit.Labels.CreateLabel(project, &gitlab.CreateLabelOptions{
		Name:  gitlab.String(name),
		Color: gitlab.String("#5843AD"),
	})

	if err != nil {
		return err
	}

	return nil
}

func DownloadRepo(folderPath, token, urlRepo string) error {
	goExecutable, err := exec.LookPath("git")
	if err != nil {
		return err
	}

	cmd := exec.Cmd{
		Path:   goExecutable,
		Args:   []string{goExecutable, "clone", fmt.Sprintf("https://oauth2:%s@%s", token, urlRepo), folderPath},
		Stdout: os.Stdout,
		Stderr: os.Stdout,
	}
	err = cmd.Run()
	if err != nil {
		return err
	}

	return nil
}
