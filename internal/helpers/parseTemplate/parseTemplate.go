package parseTemplate

import (
	"errors"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"html/template"
	"io/ioutil"
	"os"
	"strings"
)

func ParseTemplate(path string, data interface{}, logger log.Logger) error {
	path = strings.ReplaceAll(path, "\\", "/")
	fileOut := strings.ReplaceAll(path, ".tmpl", "")
	fileConvert, _ := os.Create(fileOut)

	err := template.Must(template.ParseFiles(path)).ExecuteTemplate(fileConvert, "test", data)
	if err != nil {
		er := errors.New("fail parse file")
		level.Error(logger).Log("Error", er)
		level.Error(logger).Log("Error", err)
		return er
	}

	read, err := ioutil.ReadFile(fileOut)
	if err != nil {
		return err
	}
	newContents := strings.Replace(string(read), "&lt;", "<", -1)
	newContents = strings.Replace(string(newContents), "&gt;", ">", -1)

	err = ioutil.WriteFile(fileOut, []byte(newContents), 0)
	if err != nil {
		return err
	}

	fileConvert.Close()
	os.RemoveAll(path)
	return nil
}
