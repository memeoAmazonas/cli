package entity

import "github.com/spf13/viper"

var RolUsers = "users"
var RolAdmin = "admin"

func GetKeyCliList() []string {
	return []string{
		viper.GetString("COUNTRY_LIST"),
		viper.GetString("SCOPE_LIST"),
		viper.GetString("DOMAIN_LIST_KEYS"),
		viper.GetString("DOMAIN_LIST_NAMES"),
		viper.GetString("ESUP_LIST_KEYS"),
		viper.GetString("ESUP_LIST_NAMES"),
		viper.GetString("PRDT_LIST_KEYS"),
		viper.GetString("PRDT_LIST_NAMES"),
		viper.GetString("CUST_LIST_KEYS"),
		viper.GetString("CUST_LIST_NAMES"),
		viper.GetString("OPRS_LIST_KEYS"),
		viper.GetString("OPRS_LIST_NAMES"),
		viper.GetString("RKCM_LIST_KEYS"),
		viper.GetString("RKCM_LIST_NAMES"),
		viper.GetString("MKTG_LIST_KEYS"),
		viper.GetString("MKTG_LIST_NAMES"),
		viper.GetString("CHAN_LIST_KEYS"),
		viper.GetString("CHAN_LIST_NAMES"),
	}
}
