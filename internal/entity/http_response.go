package entity

import (
	"context"
	"encoding/json"
	"net/http"
)

type ResponseHttp struct {
	Code    int         `json:"code"`
	Key     string      `json:"key,omitempty"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type RequestHttp struct {
	//Res     interface{}
	Claims  interface{}
	Params  interface{}
	Request interface{}
}

func EncodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusBadRequest)
	var key string

	switch err {
	case ErrAuth:
		key = "AUTH_ERR"
	case ErrValidation:
		key = "BODY_VALIDATION_ERR"
	case ErrFolderDelete:
		key = "AUTH_ERR_FOLDER_01"
	case ErrFolderCreate:
		key = "AUTH_ERR_FOLDER_02"
	case ErrFolderList:
		key = "FOLDER_LIST_ERR"
	case ErrFileOpen:
		key = "OPEN_FILE"
	case ErrGroupNotFound:
		key = "AUTH_ERR_GROUP"
	case ErrTemplateOpen:
		key = "TEMPLATE_ERR_OPEN"
	case ErrTemplateDownload:
		key = "TEMPLATE_ERR_DOWNLOAD"
	case ErrProjectList:
		key = "PROJECT_ERR_LIST"
	case ErrProjectCommit:
		key = "PROJECT_COMMIT_ERR"
	case ErrTmplFile:
		key = "TMPL_PARSE_ERR"
	case ErrTmplFolder:
		key = "TMPL_PARSE_FOLDER_ERR"
	case ErrRepositoryUsers:
		key = "USERS_LIST"
	case ErrRepositoryCreateUsers:
		key = "USERS_CREATE"
	case ErrRepositoryUsersNotFound:
		key = "USERS_NOT_FOUND"
	case ErrRepositoryUsersExist:
		key = "USERS_EXISTS"
	case ErrPassword:
		key = "PASSWORD_WRONG"
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"code":  http.StatusBadRequest,
		"key":   key,
		"error": err.Error(),
	})
}
