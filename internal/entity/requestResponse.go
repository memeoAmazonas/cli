package entity

import (
	"context"
	"encoding/json"
	"net/http"
)

type UserPswRequest struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

type LoginResponse struct {
	Code  string `json:"code,omitempty"`
	Error string `json:"error,omitempty"`
	Token string `json:"token,omitempty"`
}

type ListResponse struct {
	Code    string              `json:"code,omitempty"`
	Error   string              `json:"error,omitempty"`
	Payload map[string][]string `json:"payload,omitempty"`
}

type CreateResponse struct {
	Code  string `json:"code,omitempty"`
	Error string `json:"error,omitempty"`
}

type DetailTech struct {
	Compilation     string   `json:"compilation"`
	Component       []string `json:"component"`
	Dependencies    []string `json:"dependencies"`
	Feature         string   `json:"feature"`
	Framework       string   `json:"framework"`
	Language        string   `json:"language"`
	TypeApiTech     string   `json:"type_api_tech"`
	TypeBd          string   `json:"type_bd"`
	Version         string   `json:"version"`
	VersionLanguage string   `json:"version_language"`
}

type CreateRequestTech struct {
	Details  DetailTech `json:"details"`
	Type     string     `json:"type"`
	Password string     `json:"password"`
	Username string     `json:"username"`
}

type Parameters struct {
	First   string `json:"public"`
	Second  string `json:"static"`
	Three   string `json:"void"`
	Feature string `json:"feature"`
}

type UserInfoGitlab struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	Username  string `json:"username"`
	State     string `json:"state"`
	AvatarUrl string `json:"avatar_url"`
	WebUrl    string `json:"web_url"`
}

func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	switch response.(type) {
	case *ResponseHttp:
		res := response.(*ResponseHttp)
		w.WriteHeader(res.Code)
		return json.NewEncoder(w).Encode(response)

	default:
		return json.NewEncoder(w).Encode(response)
	}
}

func DecodeRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req UserPswRequest
	err := json.NewDecoder(r.Body).Decode(&req)

	usernameToken := r.Header.Get("Authorization")
	if usernameToken !=""{
		req.Username = usernameToken
	}

	if err != nil {
		return nil, err
	}
	return req, nil
}

func DecodeRequestCreate(_ context.Context, r *http.Request) (interface{}, error) {
	var req CreateRequestTech
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	return req, nil
}
