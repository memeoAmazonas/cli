package entity

import "errors"

var ErrAuth = errors.New("Auth error")

var ErrValidation = errors.New("Validation error")

var ErrFolderDelete = errors.New("Folder delete")
var ErrFolderCreate = errors.New("Folder create")
var ErrFolderList = errors.New("Folder list")

var ErrFileOpen = errors.New("Error open file")

var ErrGroupNotFound = errors.New("Group not found")

var ErrTemplateOpen = errors.New("Template open")
var ErrTemplateDownload = errors.New("Template download")

var ErrProjectList = errors.New("Error list project")
var ErrProjectCommit = errors.New("Error commit project")

var ErrTmplFile = errors.New("Tmpl parse error")
var ErrTmplFolder = errors.New("Tmpl parse folder error")

var ErrRepositoryUsers = errors.New("Error list get users")
var ErrRepositoryCreateUsers = errors.New("Error create users")
var ErrRepositoryUsersNotFound = errors.New("User not found")
var ErrRepositoryUsersExist = errors.New("User exist")
var ErrRepositoryUsersUpdate = errors.New("Error update users")
var ErrRepositoryUsersDelete = errors.New("Error delete users")

var ErrPassword = errors.New("Password no math")
var ErrPasswordEncode = errors.New("Password encode error")

var ErrConfigSave = errors.New("Error save configutarion")
