FROM golang:1.16.5

RUN mkdir /falabella

ADD . /farabela

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN go build -o ./cligitlab cmd/main.go

EXPOSE 8080

CMD ["./falabella/cligitlab"]