terraform {
  required_version = ">= 0.12"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# configuracion provider heroku
provider "aws" {
  region = var.aws_region
}

## EC2

resource "aws_vpc" "main" {
  cidr_block = "10.10.0.0/16"
}